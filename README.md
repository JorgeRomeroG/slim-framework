# Slim Framework App

Julieta Navarro Rivera

Jorge Alejandro Romero García

## Run

Move to `slim-framework`

Run `php -S localhost:8080 -t public public/index.php`

After that, open `http://localhost:8080` in your browser.